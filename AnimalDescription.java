import javafx.scene.image.Image;

public class AnimalDescription {
	
	private String name;
	private Image image;
	private int unlockLevel;
	private int cost;
	private double draw;
	
	public AnimalDescription(String name, int unlockLevel, int cost, double draw) throws Exception {
		this.name = name;
		// Only have to create one image for all animals
		this.image = new Image(getClass().getResource("/images/"+name.toLowerCase()+".png").toURI().toString());
		this.unlockLevel = unlockLevel;
		this.cost = cost;
		this.draw = draw;
	}

	public String getName() {
		return name;
	}
	
	public int getUnlockLevel() {
		return unlockLevel;
	}

	public int getCost() {
		return cost;
	}

	public Image getListSprite() {
		return image;
	}

	public double getDraw() {
		return draw;
	}

}
