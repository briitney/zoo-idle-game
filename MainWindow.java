import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainWindow extends Application {
	
	
	// Build the gui and create and run a Game
	public void start(Stage stage) throws Exception {

		BorderPane root = new BorderPane();
		Scene scene = new Scene(root);
		stage.setScene(scene);
		
		
		// My window manager uses a keybinding instead of an icon to close windows, so this is for convenience.
		Button btn = new Button("X");
		btn.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				stage.close();
			}
		});
		HBox filler = new HBox();
		HBox.setHgrow(filler, Priority.ALWAYS);
		ToolBar toolbar = new ToolBar(filler, btn);
		root.setTop(toolbar);

		ScrollPane scrollPane = new ScrollPane();
		VBox animalListPane = new VBox(10);
		animalListPane.setPadding(new Insets(10));
		scrollPane.setContent(animalListPane);
		root.setRight(scrollPane);
		
		Pane leftPane = new Pane();
		Canvas zooCanvas = new Canvas();
		zooCanvas.widthProperty().bind(leftPane.widthProperty());
		zooCanvas.heightProperty().bind(leftPane.heightProperty());
		leftPane.getChildren().add(zooCanvas);
		root.setCenter(leftPane);
		
		Game game = new Game(animalListPane, zooCanvas);
				
		game.build();
		
		stage.show();
		
		game.run();
	}


	public static void main(String[] args) {
		launch(args);
	}
}
