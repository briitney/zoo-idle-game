import java.net.URISyntaxException;
import java.util.ArrayList;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Duration;


// The Game holds consistent assets, like the sprites for the cage and clouds
// Also handles running the game loop and drawing the assets
public class Game {
	private VBox animalListPane;
	private Canvas zooCanvas;
	private double zooWidth;
	private double zooHeight;
	private GraphicsContext gc;
	private GameState state;
	private Timeline gameLoop;
	
	private Image grass;
	private Image sky;
	private ArrayList<Sprite> clouds;
	private Image cagefront;
	private Image cageback;
	
	
	// Instantiate the basic sprites
	public Game(VBox animalListPane, Canvas zooCanvas) throws URISyntaxException {
		this.animalListPane = animalListPane;
		this.zooCanvas = zooCanvas;
		this.gc = zooCanvas.getGraphicsContext2D();
		
		Image cloud = new Image(getClass().getResource("/images/cloud.png").toURI().toString());
		this.clouds = new ArrayList<Sprite>();
		for (int i = 0; i < 7; i++) {
			clouds.add(new CloudSprite(cloud, Math.random(), Math.random()));
		}
		
		this.cagefront = new Image(getClass().getResource("/images/cagefront.png").toURI().toString());
		this.cageback = new Image(getClass().getResource("/images/cageback.png").toURI().toString());
		this.grass = new Image(getClass().getResource("/images/grass.png").toURI().toString());
		this.sky = new Image(getClass().getResource("/images/sky.png").toURI().toString());
	}

	public void build() throws Exception {
		GameBuilder gameBuilder = new GameBuilder();
		gameBuilder.populate(animalListPane);
		this.state = gameBuilder.getState();
		// Need to always know the width/height of the canvas when drawing sprites
		InvalidationListener resizeListener = new InvalidationListener() {
			public void invalidated(Observable observable) {
				zooWidth = zooCanvas.getWidth();
				zooHeight = zooCanvas.getHeight();
			}
		};
		zooCanvas.widthProperty().addListener(resizeListener);
		zooCanvas.heightProperty().addListener(resizeListener);
	}
	
	public void run() {
		gameLoop = new Timeline();
		gameLoop.setCycleCount(Timeline.INDEFINITE);

		KeyFrame keyFrame = new KeyFrame(
			// ~60fps, and the logic is tied to frame to simplify calucations.
			Duration.seconds(.017),
			new EventHandler<ActionEvent>() {
				int frameNo = 0;
				public void handle(ActionEvent event) {
					
					// Most of this is just saying to draw things on the canvas
					gc.drawImage(sky, 0, 0, zooWidth, zooHeight);
					for (Sprite cloud : clouds) {
						cloud.draw(gc, zooWidth, zooHeight);
						cloud.move();
					}
					gc.drawImage(grass, 0, 0, zooWidth, zooHeight);
					gc.drawImage(cageback, 0, 0, zooWidth, zooHeight);
					for (Sprite animalSprite : state.getAnimalSprites()) {
						animalSprite.draw(gc, zooWidth, zooHeight);
						animalSprite.move();
					}
					gc.drawImage(cagefront, 0, 0, zooWidth, zooHeight);
					for (Sprite person : state.getPeople()) {
						person.draw(gc, zooWidth, zooHeight);
						person.move();
					}
					
					Text next = new Text("Level: "+state.getCurrentLevel());
					Text exp = new Text("Exp to next: "+(state.getExpToLevel(state.getCurrentLevel()+1)-state.getExperience()));
					Text money = new Text("Money: "+String.format("%.2f", state.getMoney()));
					Text customers = new Text("Customers: "+state.getPeople().size());
					int textHeight = (int) next.getLayoutBounds().getHeight();
					
					gc.fillText(next.getText(), zooWidth-next.getLayoutBounds().getWidth()-20, 20);
					gc.fillText(exp.getText(), zooWidth-exp.getLayoutBounds().getWidth()-20, 25+textHeight);
					gc.fillText(money.getText(), zooWidth-money.getLayoutBounds().getWidth()-20, 30+2*textHeight);
					gc.fillText(customers.getText(), zooWidth-customers.getLayoutBounds().getWidth()-20, 35+3*textHeight);
					
					frameNo++;
					// Update the state 2x a second
					if (frameNo % 30 == 0) {
						state.update();
						frameNo = frameNo % 60;
					}
				}
			}
		);
        gameLoop.getKeyFrames().add(keyFrame);
        gameLoop.play();
	}
}
