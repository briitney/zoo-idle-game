import javafx.scene.image.Image;

public class CloudSprite extends WrapSprite {
	
	public CloudSprite(Image image, double x, double y) {
		// Put cloud somewhere above the grassline
		super(image, x, y,
			Math.random()*0.001+0.0005,
			0, 0,
			1, 0.25);
	}
}
