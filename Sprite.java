import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

abstract public class Sprite {
	// x and y as percentages between 0.0 and 1.0
	protected double x;
	protected double y;
	
	protected double acceleration;
	protected double xStart;
	protected double yStart;
	protected double xEnd;
	protected double yEnd;
	
	// imageWidth and imageHeight are as percentages of the canvas.
	protected double imageWidth;
	protected double imageHeight;
	protected Image image;
	
	public Sprite(Image image, double x, double y, double acceleration,  double xStart, double yStart, double xEnd, double yEnd) {
		this.x = x;
		this.y = y;
		this.acceleration = acceleration;
		this.xStart = xStart;
		this.yStart = yStart;
		this.xEnd = xEnd;
		this.yEnd = yEnd;
		this.image = image;
		
		// 500 and 350 are magic numbers for the size of the original background
		this.imageWidth = image.getWidth()/500.0;
		this.imageHeight = image.getHeight()/350.0;
	}

	abstract public void move();

	abstract public void draw(GraphicsContext gc, double canvasWidth, double canvasHeight);
	
	public double getx () {
		return this.x;
	}

	public double gety() {
		return this.y;
	}
}
