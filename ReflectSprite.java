import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class ReflectSprite extends Sprite {
	
	protected Boolean goingRight;

	public ReflectSprite(Image image, double x, double y, double acceleration, double xStart, double yStart,
			double xEnd, double yEnd) {
		super(image, x, y, acceleration, xStart, yStart, xEnd, yEnd);
		this.goingRight = true;
	}
	
	// Sprite turns once it hits an edge
	public void move() {
		if (goingRight) {
			this.x = (this.x+this.acceleration);
			if (this.x > 1) goingRight =  false;
		} else {
			this.x = (this.x-this.acceleration);
			if (this.x < 0) goingRight =  true;
		}
	}
	
	// Draw the sprite facing one direction or the other
	public void draw(GraphicsContext gc, double canvasWidth, double canvasHeight) {
		if (goingRight) {
			gc.drawImage(image,
				canvasWidth*(xStart+x*((xEnd-imageWidth)-xStart)+imageWidth),
				canvasHeight*(yStart+y*(yEnd-yStart)-imageHeight),
				-imageWidth*canvasWidth,
				imageHeight*canvasHeight);
		} else {
			gc.drawImage(image,
				canvasWidth*(xStart+x*((xEnd-imageWidth)-xStart)),
				canvasHeight*(yStart+y*(yEnd-yStart)-imageHeight),
				imageWidth*canvasWidth,
				imageHeight*canvasHeight);
		}
	}

}
