import javafx.scene.image.Image;

public class AnimalSprite extends ReflectSprite {
	private int frameNo;

	public AnimalSprite(Image image, double x, double y) {
		// Put the animal somewhere inside the cage
		super(image, x, y,
			0.01+Math.random()*0.01, 
			0.2+x*0.6-0.05, 0.35,
			0.2+x*0.6+0.05, 0.55);
		this.frameNo = 0;
		this.y += 0.02;
	}
	
	// Create the up and down bouncing effect
	public void move() {
		frameNo++;
		if (frameNo % 40 == 0) {
			y -= 0.02;
			frameNo = 0;
		} else if (frameNo % 30 == 0) {
			y -= 0.02;
		} else if (frameNo % 20 == 0) {
			y += 0.02;
		} else if (frameNo % 10 == 0) {
			y += 0.02;
		}
		super.move();
	}

}
