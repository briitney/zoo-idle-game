import java.util.ArrayList;

// An ArrayList that orders elements by their y position.
public class OrderedSpriteList extends ArrayList<Sprite> {
	private static final long serialVersionUID = -3433992952347829370L;
	
	public boolean add(Sprite sprite) {
		int l = 0, r = this.size()-1, m = 0;
		while (l <= r) {
			m = (l+r)/2;
			if (this.get(m).gety() < sprite.gety()) l = m + 1;
			else r = m - 1;
		}
		super.add(l, sprite);
		return true;
	}
}
