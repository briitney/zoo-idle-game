import java.util.ArrayList;

import javafx.beans.property.SimpleIntegerProperty;


// Class that both holds general information about a type of animal as well as a list of sprites of that animal type.
public class Animal {

	private AnimalDescription animalDescription;
	private SimpleIntegerProperty quantity = new SimpleIntegerProperty(0);
	private ArrayList<Sprite> animalSprites;
	private ArrayList<ArrayList<Sprite>> listeningLists;

	public Animal(String name, int unlockLevel, int cost, double draw) throws Exception {
		this.animalDescription = new AnimalDescription(name, unlockLevel, cost, draw);
		this.animalSprites = new ArrayList<Sprite>();
		this.listeningLists = new ArrayList<ArrayList<Sprite>>();
	}

	public void addList(ArrayList<Sprite> list) {
		listeningLists.add(list);
	}
	
	public AnimalDescription getAnimalDescription() {
		return this.animalDescription;
	}

	public Integer getQuantity() {
		return quantity.getValue();
	}
	
	public ArrayList<Sprite> getAnimalSprites() {
		return this.animalSprites;
	}

	// Create a new sprite every time the quantity is incremented
	public void incrementQuantity() {
		AnimalSprite newSprite = new AnimalSprite(animalDescription.getListSprite(), Math.random(), Math.random());
		this.animalSprites.add(newSprite);
		for (ArrayList<Sprite> list : listeningLists) {
			list.add(newSprite);
		}
		this.quantity.set(this.quantity.add(1).intValue());
	}

	public SimpleIntegerProperty getObservableQuantity() {
		return quantity;
	}

}
