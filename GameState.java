import java.net.URISyntaxException;
import java.util.ArrayList;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.image.Image;

// Keep track of game stats, animals, and people
public class GameState {
	
	private int experience;
	private SimpleIntegerProperty currentLevel = new SimpleIntegerProperty(1);
	private double money;
	private ArrayList<Animal> animals;
	private Image person1;
	private Image person2;
	private ArrayList<Sprite> people;
	private ArrayList<Sprite> animalSprites;

	public GameState() throws URISyntaxException {
		this.experience = 0;
		this.money = 0;
		this.animals = new ArrayList<Animal>();
		
		person1 = new Image(getClass().getResource("/images/person1.png").toURI().toString());
		person2 = new Image(getClass().getResource("/images/person2.png").toURI().toString());
		this.people = new OrderedSpriteList();
		this.animalSprites = new OrderedSpriteList();;
	}

	public void addAnimal(String name, int unlockLevel, int cost, double draw) throws Exception {
		Animal newAnimal = new Animal(name, unlockLevel, cost, draw);
		animals.add(newAnimal);
		// Have one big list of the animal's sprites so the order of the y overlap makes sense
		newAnimal.addList(animalSprites);
	}

	public int getExperience() {
		return experience;
	}
	
	public void addExperience(int experience) {
		this.experience += experience;
		if (getLevelFromExp() > this.currentLevel.getValue()) {
			this.incrementLevel();
		}
	}

	private void incrementLevel() {
		this.currentLevel.set(this.currentLevel.add(1).intValue());
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double d) {
		this.money = d;
	}
	
	public ArrayList<Animal> getAnimals() {
		return animals;
	}
	
	public ArrayList<Sprite> getPeople() {
		return people;
	}
	
	public void addPerson() {
		this.people.add(new PersonSprite(person1, person2, 0, Math.random()));
	}
	
	// Add make sure the list holds the correct number of people, even if it means incrementing by more than one
	public void balancePeople(double numOfPeople) {
		while (numOfPeople > people.size()) {
			addPerson();
		}
	}
	
	// Level is determined as a function of experience
	public int getLevelFromExp() {
		return (int) (Math.log(experience+1)+1);
	}
	
	// Inverse of the above function
	public int getExpToLevel(int level) {
		return (int) (Math.exp(level-1)-1);
	}

	public int getCurrentLevel() {
		return this.currentLevel.getValue();
	}

	public SimpleIntegerProperty getObservableCurrentLevel() {
		return this.currentLevel;
	}

	// Every time the state is updated, see how many people the animals are drawling in.
	// Then use the number of people to calculate the money/experience gained and increment the level if necessary 
	public void update() {
		double numOfPeople = 0;
		for (Animal animal: animals) {
			numOfPeople += animal.getAnimalDescription().getDraw()*animal.getAnimalSprites().size();
		}
		balancePeople(numOfPeople);
		numOfPeople = Math.ceil(numOfPeople);
		money += numOfPeople/2.0;
		experience += numOfPeople;
		if (getLevelFromExp() > currentLevel.intValue()) {
			while (getLevelFromExp() > currentLevel.intValue()) {
				incrementLevel();
			}
		}
	}

	public ArrayList<Sprite> getAnimalSprites() {
		return animalSprites;
	}
}
