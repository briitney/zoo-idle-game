import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class WrapSprite extends Sprite {

	public WrapSprite(Image image, double x, double y, double acceleration, double xStart, double yStart, double xEnd,
			double yEnd) {
		super(image, x, y, acceleration, xStart, yStart, xEnd, yEnd);
	}

	// Sprite wraps around the canvas
	public void move() {
		x += acceleration;
		if (x > xEnd) {
			x = xStart+x%xEnd;
		}
	}

	public void draw(GraphicsContext gc, double canvasWidth, double canvasHeight) {
		gc.drawImage(image,
			canvasWidth*(xStart+x*((xEnd+imageWidth)-xStart)-imageWidth),
			canvasHeight*(yStart+y*((yEnd+imageHeight)-yStart)-imageHeight),
			imageWidth*canvasWidth,
			imageHeight*canvasHeight);
	}

}
