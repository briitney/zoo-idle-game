import javafx.scene.layout.VBox;

public class GameBuilder {
	private GameState state;
	
	// Load some information into the game state
	public GameBuilder() throws Exception {
		this.state = new GameState();
		this.state.setMoney(20);
		this.state.addAnimal("Butterfly", 1, 10, 0.1);
		this.state.addAnimal("Snake", 3, 15, 0.2);
		this.state.addAnimal("Duck", 6, 25, 0.4);
		this.state.addAnimal("Pig", 7, 35, 0.6);
		this.state.addAnimal("Sheep", 8, 45, 0.8);
		this.state.addAnimal("Goat", 9, 55, 1.0);
	}

	// For every animal in the game state, create a button that effects it
	public void populate(VBox animalListPane) {
		for (Animal animal : state.getAnimals()) {
			animalListPane.getChildren().add(new AnimalButton(state, animal));
		}
	}

	public GameState getState() {
		return state;
	}

}
