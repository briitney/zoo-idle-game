# Zoo Idle Game
This is a project where the main goal is to learn how to use the JavaFX framework. The visual assets were created by myself, and although I know a little bit about how to get around an image editor, I'm not an artist.

The game itself is rather simple, where there is a zoo containing animals (all in one cage, which may be a problem in the real world, but not in the game world) the people pay to go see. The gameplay consists of jsut buying new animals with your profit. Popular games in this genre include Cookie Clicker and AdVenture Capitalist.