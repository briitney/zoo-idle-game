import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;


// Entire user interaction is in these buttons.
public class AnimalButton extends BorderPane {
	
	private GameState state;
	private Animal animal;
	private AnimalDescription animalDescription;

	private static Background lightGreyBG = new Background(
			new BackgroundFill(Color.LIGHTGREY, CornerRadii.EMPTY, Insets.EMPTY));
	private static Background darkGreyBG = new Background(
			new BackgroundFill(Color.DARKGREY, CornerRadii.EMPTY, Insets.EMPTY));
	private static Background redBG = new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY));

	public AnimalButton(GameState state, Animal animal) {
		this.state = state;
		this.animal = animal;
		this.animalDescription = animal.getAnimalDescription();
		
		this.setPadding(new Insets(10));
		
		// Check if an animal is already unlocked (happens if animal is available at level 1)
		// or needs to listen for when it's unlocked
		if (animalDescription.getUnlockLevel() <= state.getCurrentLevel()) {
			this.constructButton();
		} else {
			this.setLeft(new Label("Unlock "+animalDescription.getName()+" at Level "+animalDescription.getUnlockLevel()));
			setBackground(darkGreyBG);
			state.getObservableCurrentLevel().addListener(makeLevelListener());
		}
	}
	
	// If the user levels up to the level to unlock the animal, change the button to reflect that.
	private ChangeListener<Number> makeLevelListener() {
		return new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> observableValue, Number oldLevel,
					Number newLevel) {
				if (animalDescription.getUnlockLevel() <= newLevel.intValue()) constructButton();
			}
		};
	}

	// Actually construct the button to be added to the gui
	public void constructButton() {
		setBackground(null);
		BorderPane animalPicWrapper = new BorderPane();
		animalPicWrapper.setPrefWidth(40);
		ImageView animalPic = new ImageView(animalDescription.getListSprite());
		if (animalPic.getImage().getWidth() > 30 || animalPic.getImage().getHeight() > 30) {
			animalPic.setFitWidth(30);
			animalPic.setFitHeight(30);
		}
		animalPic.setPreserveRatio(true);
        animalPic.setSmooth(true);
		animalPicWrapper.setCenter(animalPic);
		HBox spacer = new HBox();
		spacer.setMinWidth(10);
		animalPicWrapper.setRight(spacer);
		this.setLeft(animalPicWrapper);
		
		// Listen to how many animals there are and update the value as needed.
		Label quantityLabel = new Label("\tQuantity: " + animal.getQuantity());
		ChangeListener<Number> quantityListener = new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observableValue, Number a,
					Number b) {
				quantityLabel.setText("\tQuantity: " + animal.getQuantity());
			}
		};
		
		animal.getObservableQuantity().addListener(quantityListener);
		this.setCenter(
				new VBox(new Label(animalDescription.getName()), new HBox(new Label("Cost: " + animalDescription.getCost()), quantityLabel)));

		// Give user visual feedback on their clicks, and show if they don't have enough money for the purchase.
		this.setOnMousePressed(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				if (state.getMoney() - animalDescription.getCost() >= 0) {
					state.setMoney(state.getMoney() - animalDescription.getCost());
					animal.incrementQuantity();
					setBackground(lightGreyBG);
				} else {
					setBackground(redBG);
				}
			}
		});

		this.setOnMouseReleased(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				setBackground(null);
			}
		});
	}

}
