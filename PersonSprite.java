import javafx.scene.image.Image;

public class PersonSprite extends ReflectSprite {
	private int frameNo;
	private Image image1;
	private Image image2;
	
	public PersonSprite(Image image1, Image image2, double x, double y) {
		// Place people somewhere under the cage
		super(image1, x, y,
			Math.random()*0.0015+0.0015,
			0, 0.7,
			1, 1);
		this.x -= this.imageWidth;
		this.frameNo = 0;
		this.image1 = image1;
		this.image2 = image2;
	}
	
	// Have the person's sprite change every 3d of a second
	public void move() {
		frameNo++;
		if (frameNo%40 == 0) {
			image = image1;
			frameNo = 0;
		} else if (frameNo % 20 == 0) {
			image = image2;
		}
		super.move();
	}
}
